<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('blogs', function (Blueprint $table){
				 $table->increments('blog_id');

				 $table->string('title',200);
				 $table->description('blog_text',200);
				 $table->string('image_name',200);
				 
                 $table->timestamps();
		 });	 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists('blogs');
    }
}
