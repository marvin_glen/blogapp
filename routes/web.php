<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view::make('pages.index');
});

Route::get('/', function () {
    return view::make('pages.blogs');
});

Route::get('/', function () {
    return view::make('pages.comments');
});

Route::resource('blogposts', 'BlogPostsController');

Route::resource('comments', 'CommentController');

Route::resource('blogs.comments', 'BlogCommentController'); /*comments are nested routes inside blogs*/